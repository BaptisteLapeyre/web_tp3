import React, {useState} from 'react'
import { Form, Field } from "react-final-form";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

export default function login() {


    const onSubmit = event => {

        const lInit = {
            method: 'PUT',
            headers: {
             'Content-type': 'application/json; charset=UTF-8'
            },
            body: JSON.stringify(event)
        }
        fetch('https://virtserver.swaggerhub.com/L3MIASHS/LOS/1.0.0/user', lInit)
        .then((response) =>
        {
            if(response.status === 201){
                window.alert("votre compte a bien été créé!");
            }
        })
    }


    // if (this.state.valide){
    //     return (<div>enregistrer</div>)
    // }else{
        return (
            <div>
                <h1>Inscription</h1>
                <div class="container d-flex justify-content-center ">

                <Form onSubmit={onSubmit}
                    render={({ handleSubmit }) => (
                        <form className=" col-8 form  border mt-5 p-3 border-primary" onSubmit={handleSubmit}>
                            <div className="form-group ">
                                <label for="exampleInputEmail1">Email address</label>
                                <Field component="input" type="email" name="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
                            </div>
                            <div className="form-group justify-content-center">
                                <label for="exampleInputPassword1">Password</label>
                                <Field name="password" type="password"  component="input" className="form-control " id="exampleInputPassword1" placeholder="Password"/>
                            </div>
                            <div className="form-group justify-content-center">
                                <label for="exampleInputPassword2">Confirmer votre password</label>
                                <Field name="password2" type="password" component="input" className="form-control " id="exampleInputPassword2" placeholder="Password"/>
                            </div>
                            <button className="btn btn-primary" type="submit">Valider</button>
                        </form>
                    )} />
            </div>
            </div>

        );
    // }
}
