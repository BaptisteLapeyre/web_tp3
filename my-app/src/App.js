import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Login from './login';
import Register from './register';

function inscription() {
  return (<Register></Register>)
}
function connect() {
  return (<Login></Login>)
}

function App() {
  return (
    <Router>
      <div className="App">
        <nav className="navbar navbar-dark bg-dark">
          <a className="navbar-brand" href="#">
            <img src="./image/Banniere.PNG" width="90" height="30" className="d-inline-block align-top" alt="" />

            <p>League of Stones</p>
          </a>
          <form className="form-inline justify-content-end">
            <Link to="/connect/"><button class="btn btn-outline-secondary" type="button">Se connecter</button></Link>
            <Link to="/inscription/"><button class="btn btn-outline-secondary" type="button">S'inscrire</button></Link>
          </form>
        </nav>
        <Route path="/inscription/" component={inscription} />
        <Route path="/connect/" component={connect} />
      </div>
    </Router>
  );
}

export default App;


